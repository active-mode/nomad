# NOMAD: A microscopic pedestrian simulation model

The Python implementation of NOMAD (see https://doi.org/10.4233/uuid:b65e6e12-a85e-4846-9122-0bb9be47a762 for details)

## How to install NOMAD

<code>pip install git+https://gitlab.tudelft.nl/active-mode/nomad.git</code>

## How to use NOMAD

Work in progress.

### Setup a simulation

Work in progress.

### Run a simulation

Work in progress.

### Process the results

Work in progress.

## How to cite NOMAD

Sparnaaij, M., Campanella, M. C., Duives, D.C., Daamen, W. & Hoogendoorn, S. P. (2023). NOMAD: A microscopic pedestrian simulation model (Version 1.0) [Software]. https://gitlab.tudelft.nl/active-mode/nomad

## Acknowledgements

Technische Universiteit Delft hereby disclaims all copyright interest in the
program “NOMAD” A microscopic pedestrian simulation model
written by the Author(s).
Jan Dirk Jansen, Dean of Civil Engineering and Geosciences 