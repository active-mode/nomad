'''
Created on 25-aug-2020

@author: Martijn Sparnaaij
'''
import argparse
import glob
import os
from pathlib import Path
import shutil
import subprocess
import sys

from NOMAD.nomad_model import createAndRunNOMADmodel
from NOMAD.simple_visualisation import visualize


RUN_ACTION_NM = 'run'
RUN_ACTION_ALIAS = 'r'
VISUALIZE_ACTION_NM = 'visualize'
VISUALIZE_ACTION_ALIAS = 'v'
EXAMPLES_ACTION_NM = 'examples'
EXAMPLES_ACTION_ALIAS = 'e'


def run():
    actionFcn, inputArgs = parseInputArgs()
    actionFcn(*inputArgs)
    
def openExamplesFolder():
    local_examples_dir = Path('~/nomad/examples').expanduser()
    if not local_examples_dir.is_dir():
        exts = ['.py', '.xml']
        local_examples_dir.mkdir(parents=True)
        examples_dir =  Path(__file__).parent.parent.joinpath('examples')
        for ext in exts:
            for file in glob.glob(str(examples_dir.joinpath(f'*{ext}'))):
                shutil.copy2(file,local_examples_dir)
    if sys.platform=='win32':
        os.startfile(local_examples_dir)
    elif sys.platform=='darwin':
        subprocess.Popen(['open', local_examples_dir])
    else:
        subprocess.Popen(['xdg-open', local_examples_dir])
    
def parseInputArgs(*args):
    main_parser = argparse.ArgumentParser(description='NOMAD model arguments') # , formatter_class=RawTextHelpFormatter
    subparsers = main_parser.add_subparsers(dest='action')
    
    parser_run = subparsers.add_parser(RUN_ACTION_NM, help='run a model from a configuration file', aliases=[RUN_ACTION_ALIAS])
    parser_run.add_argument('file', type=str, help='a xml scenario file')

    parser_visualize = subparsers.add_parser(VISUALIZE_ACTION_NM, help='visualize results from an output file', aliases=[VISUALIZE_ACTION_ALIAS])
    parser_visualize.add_argument('file', type=str, help='a .scen results file')
    
    subparsers.add_parser(EXAMPLES_ACTION_NM, help='open and view the examples folder', aliases=[EXAMPLES_ACTION_ALIAS])

    if len(args):
        res = main_parser.parse_args(args)
    else:
        res = main_parser.parse_args()
    
    if res.action in [RUN_ACTION_NM, RUN_ACTION_ALIAS]:
        actionFcn = createAndRunNOMADmodel
        args_out = (res.file,)
    elif res.action in [VISUALIZE_ACTION_NM, VISUALIZE_ACTION_ALIAS]:
        actionFcn = visualize
        args_out = (res.file,)
    else:
        actionFcn = openExamplesFolder
        args_out = ()
    
    return actionFcn, args_out

if __name__ == "__main__":
    run()