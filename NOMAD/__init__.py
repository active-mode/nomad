
import ctypes
from pathlib import Path
import platform
import logging.config
from logging.handlers import RotatingFileHandler

class MakeRotatingFileHandler(RotatingFileHandler):
    def __init__(self, filename, *args, **kwargs):
        filenamePath = Path(filename).resolve()
        filenamePath.parent.mkdir(parents=True, exist_ok=True)
        super().__init__(filename, *args, **kwargs)
        
logging_config_file = Path(__file__).parent.parent.joinpath('logging.ini')
print(logging_config_file.resolve())
logging.config.fileConfig(logging_config_file, disable_existing_loggers=True)

_package_path = Path(__file__).parent.resolve()
_c_path = _package_path.joinpath('C')

if platform.system() == 'Darwin':
    ext = 'dylib'
else:
    ext = 'so'

getInteractionData_so_file = _c_path.joinpath('getInteractionData.' + ext)
calcPedForces_so_file = _c_path.joinpath('calcPedForces.' + ext)
convergeCostMatrix_so_file = _c_path.joinpath('convergeCostMatrix.' + ext)

try:
    ctypes.CDLL(str(getInteractionData_so_file))
    canUseGetInteractionCompiledCode = True
except:
    canUseGetInteractionCompiledCode = False

try:
    ctypes.CDLL(str(calcPedForces_so_file))
    canUseCalcPedForcesCompiledCode = True
except:
    canUseCalcPedForcesCompiledCode = False

try:
    ctypes.CDLL(str(convergeCostMatrix_so_file))
    canUseConvergeCostMatrixCompiledCode = True
except:
    canUseConvergeCostMatrixCompiledCode = False    
    
import numpy as np
NOMAD_RNG = np.random.default_rng()

LOCAL_DIR = Path('~/nomad/').expanduser()