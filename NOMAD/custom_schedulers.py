'''
Created on 7 Jun 2021

@author: martijnsparnaa
'''
from types import SimpleNamespace
from xml.dom.minidom import parse

import NOMAD.xml_functions as xml


def createScheduler(configFlNm, destinations, pedParameterSets, sources, activities, walkLevels):
    raise Exception('Implement function!')

class CustomScheduler():
    '''
    classdocs
    '''

    def createDemandPatterns(self):
        raise Exception('Implement function!')
    
    def getDynamicSchedulers(self):
        raise Exception('Implement function!')
    
    
def getInputFromXml(xmlFlNm, elementDefinition):
    DOMTree = parse(str(xmlFlNm))
    schedulerXml = DOMTree.documentElement
    schedulerInputDict = xml.getElementDict(schedulerXml, elementDefinition)
    return SimpleNamespace(**schedulerInputDict)
    